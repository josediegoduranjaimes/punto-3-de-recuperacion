/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author PERSONAL
 * @param <T>
 */
public class Listacd<T> {

    private NodoD<T> cabecera;
    private int tamano = 0;

    public Listacd() {
        this.cabecera = new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSiguiente(this.cabecera);
        this.cabecera.setAnterior(cabecera);

    }

    public int getTamano() {
        return tamano;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD();
        nuevo.setInfo(info);
        nuevo.setSiguiente(this.cabecera.getSiguiente());
        //El anterior de nuevo nodo ES la cabecera
        nuevo.setAnterior(this.cabecera);
        //El siguiente de cabecera es el nuevo nodo
        this.cabecera.setSiguiente(nuevo);
        //El siguiente del nuevo nodo SU anterior ES el nuevo nodo
        nuevo.getSiguiente().setAnterior(nuevo);
        //Aumentar la cardinalidad
        this.tamano++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD();
        nuevo.setInfo(info);
        //El anterior de nuevo es el anterior de cabecera
        nuevo.setAnterior(this.cabecera.getAnterior());
        //El siguiente de nuevo es cabecera
        nuevo.setSiguiente(this.cabecera);
        //El anterior de cabecera su siguiente ES nuevo nodo
        this.cabecera.getAnterior().setSiguiente(nuevo);
        //El anterior de cabecera es ahora nuevo
        this.cabecera.setAnterior(nuevo);
        //Aumento cardinalidad
        this.tamano++;
    }

    public boolean esVacia() {
        // Método 1: tamano ==0 
        // Método 2:
        return this.cabecera == this.cabecera.getSiguiente() && this.cabecera == this.cabecera.getAnterior();
    }

    @Override
    public String toString() {
        String msg = "ListaCD{";

        for (NodoD<T> x = this.cabecera.getSiguiente(); x != this.cabecera; x = x.getSiguiente()) {
            msg += x.getInfo().toString() + "<-->";
        }

        return msg + "}";
    }

    public T eliminar(int pos) {
        /*
        Comprobar que pos (posición) sea válida  >0 && < cardinalidad xxx
        Buscar el nodo actual dada la posición  getPos(…) xxxx
        Coloco nodo anterior = actual.getAnt() xxxx
        --->
        nodoAnt su siguiente Es el siguiente de nodo actual xxx
        El siguiente de nodo actual su anterior es nodoAnt
        Cardinilidad –
        desunir nodoActual (free(..))
        Retornar el info del nodo actual
         */
        try {
            NodoD<T> nodoActual = this.getPos(pos);
            NodoD<T> nodoAnt = nodoActual.getAnterior();
            nodoAnt.setSiguiente(nodoActual.getSiguiente());
            nodoActual.getSiguiente().setAnterior(nodoAnt);
            this.tamano--;
            this.desUnir(nodoActual);
            return nodoActual.getInfo();

        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }
    
    public void insertarOrdenado(T info) {
        if (this.esVacia()) {
            this.insertarInicio(info);
        } else {
            NodoD<T> x = this.cabecera;
            NodoD<T> y = x;
            x = x.getSiguiente();
            while (x != this.cabecera) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSiguiente();
            }
            if (x == cabecera.getSiguiente()) {
                this.insertarInicio(info);
            } else {
                y.setSiguiente(new NodoD<>(info, x, y));
                x.setAnterior(y.getSiguiente());
                this.tamano++;
            }
        }
    }


    private void desUnir(NodoD<T> x) {
        x.setAnterior(x); //x.setAnt(null)
        x.setSiguiente(x);////x.setSig(null)
    }

    private NodoD<T> getPos(int pos) throws Exception {

        if (pos < 0 || pos >= this.tamano) {
            throw new Exception("La posición " + pos + " no es válida en la lista");
        }
        NodoD<T> nodoPos = this.cabecera.getSiguiente();
        while (pos-- > 0) {
            nodoPos = nodoPos.getSiguiente();
        }
        return nodoPos;

    }

}