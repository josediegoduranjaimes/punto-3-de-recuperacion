 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author PERSONAL
 * @param <T>
 */
public class NodoD<T> {

    private T info;
    private NodoD<T> siguiente, anterior;

    public NodoD() {
    }

    public NodoD(T info, NodoD<T> siguiente, NodoD<T> anterior) {
        this.info = info;
        this.siguiente = siguiente;
        this.anterior = anterior;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public NodoD<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoD<T> siguiente) {
        this.siguiente = siguiente;
    }

    public NodoD<T> getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoD<T> anterior) {
        this.anterior = anterior;
    }

}
