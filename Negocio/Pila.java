/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author PERSONAL
 * @param <T>
 */
public class Pila<T> {

    private Listacd<T> tope;

    public Pila() {
        this.tope = new Listacd();
    }

    public void push(T elemento) {
        this.tope.insertarInicio(elemento);
    }

    public T pop() {
        return this.tope.eliminar(0);
    }

    public boolean esVacia() {
        return this.tope.esVacia();
    }

    public int getTamano() {
        return this.tope.getTamano();
    }
    
    public boolean contains(T elemento) throws Exception { // Evaluar si el elemento se encuentra dentro de la pila
        if(this.esVacia()) throw new Exception("La pila a evaluar está vacía.");
        boolean validar = false;
        try{
            Pila<T> temp = new Pila<>();
            while(!this.esVacia()) {
                T info = this.pop();
                if(elemento.equals(info)) validar = true;
                temp.push(info);
            }
            while(!temp.esVacia()) this.push(temp.pop());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return validar;
    }

}