/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import java.util.List;

/**
 *
 * @author PERSONAL
 */
public class SistemaNotas {
    
    public void notaAprovatoria(){
        float notaaprovatoria;
    }
    
    public void crearPilaEstudiantes(List<Estudiante> list, Pila<Estudiante> pila){
        list.forEach((e) -> {
            pila.push(e);
        });
    }
    
    public void dosMaterias(Pila<Estudiante> p1, Pila<Estudiante> p2, Pila<Estudiante> p3, List<Estudiante> list) throws Exception{ // Vieron solo 2 materias
        if(p1.esVacia() || p2.esVacia() || p3.esVacia() || list.isEmpty()) throw new Exception("Verifique nuevamente las listas");
        System.out.println("Estudiantes que están matriculados en 2 materias: ");
        for(Estudiante e : list){
            int contador = 0;
            if(p1.contains(e)) contador++;
            if(p2.contains(e)) contador++;
            if(p3.contains(e)) contador++;
            
            if(contador == 2) System.out.println(e.getNombres());
        }
    }
    
    public void unaMateria(Pila<Estudiante> p1, Pila<Estudiante> p2, Pila<Estudiante> p3, List<Estudiante> list) throws Exception{ // Vieron solo 1 materia1
        if(p1.esVacia() || p2.esVacia() || p3.esVacia() || list.isEmpty()) throw new Exception("Verifique nuevamente las listas");
        System.out.println("Estudiantes que están matriculados en una sola materia: ");
        for(Estudiante e : list){
            int contador = 0;
            if(p1.contains(e)) contador++;
            if(p2.contains(e)) contador++;
            if(p3.contains(e)) contador++;
            
            if(contador == 1) System.out.println(e.getNombres());
        }
    }
    
    public void reprobaronTodas(Pila<Estudiante> p1, Pila<Estudiante> p2, Pila<Estudiante> p3, List<Estudiante> list) throws Exception{ // Reprobaron las 3 materias
        if(p1.esVacia() || p2.esVacia() || p3.esVacia() || list.isEmpty()) throw new Exception("Verifique nuevamente las listas");
        System.out.println("Estudiantes que perdieron las 3 materias: ");
        for(Estudiante e : list){
            boolean b1 = false, b2 = false, b3 = false;
            if(p1.contains(e) && p2.contains(e) && p3.contains(e)){
                Pila<Estudiante> pila1 = new Pila<>(), pila2 = new Pila<>(), pila3 = new Pila<>();
                while(!p1.esVacia()){
                    Estudiante e1 = p1.pop();
                    pila1.push(e1);
                    if(e1.equals(e)) b1 = e1.aprueba();
                }
                
                while(!pila1.esVacia()) p1.push(pila1.pop());
                
                while(!p2.esVacia()){
                    Estudiante e1 = p2.pop();
                    pila2.push(e1);
                    if(e1.equals(e)) b2 = e1.aprueba();
                }
                
                while(!pila2.esVacia()) p2.push(pila2.pop());
                
                while(!p3.esVacia()){
                    Estudiante e1 = p3.pop();
                    pila3.push(e1);
                    if(e1.equals(e)) b3 = e1.aprueba();
                }
                
                while(!pila3.esVacia()) p3.push(pila3.pop());
                
                if(!b1 && !b2 && !b3) System.out.println("Código: " + e.getCodigo());
            }
        }
    }
    
    public Listacd aprobaronTodas(Pila<Estudiante> p1, Pila<Estudiante> p2, Pila<Estudiante> p3, List<Estudiante> list) throws Exception{ // Estudiantes que aprobaron las 3 materias
        if(p1.esVacia() || p2.esVacia() || p3.esVacia() || list.isEmpty()) throw new Exception("Verifique nuevamente las listas");
        Listacd result = new Listacd();
        for(Estudiante e : list){
            boolean b1 = false, b2 = false, b3 = false;
            if(p1.contains(e) && p2.contains(e) && p3.contains(e)){
                Pila<Estudiante> pila1 = new Pila<>(), pila2 = new Pila<>(), pila3 = new Pila<>();
                while(!p1.esVacia()){
                    Estudiante e1 = p1.pop();
                    pila1.push(e1);
                    if(e1.equals(e)) b1 = e1.aprueba();
                }
                
                while(!pila1.esVacia()) p1.push(pila1.pop());
                
                while(!p2.esVacia()){
                    Estudiante e1 = p2.pop();
                    pila2.push(e1);
                    if(e1.equals(e)) b2 = e1.aprueba();
                }
                
                while(!pila2.esVacia()) p2.push(pila2.pop());
                
                while(!p3.esVacia()){
                    Estudiante e1 = p3.pop();
                    pila3.push(e1);
                    if(e1.equals(e)) b3 = e1.aprueba();
                }
                
                while(!pila3.esVacia()) p3.push(pila3.pop());
                
                if(b1 && b2 && b3){
                    result.insertarOrdenado(e.getCodigo());
                }
            }
        }
        return result;
    }
    
    public Listacd crearListaCD(Pila<Estudiante> materia){ // ListaCD con los estudiantes que perdieron p1 pero no p2
        Listacd list = new Listacd();
        Pila<Estudiante> p = new Pila<>();
        while(!materia.esVacia()){
            Estudiante e = materia.pop();
            if(e.getPrevio1() < 3 && e.getPrevio2() >= 3) list.insertarFin(e);
            p.push(e);
        }
        
        while(!p.esVacia()) materia.push(p.pop());
        return list;
    }
    
}
