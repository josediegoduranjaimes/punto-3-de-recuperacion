/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author PERSONAL
 */
public class Estudiante {
    
    int codigo;
    String nombres;
    float previo1,previo2,previo3;
    float examenfinal; 

    public Estudiante() {
    }

    public Estudiante(int codigo, String nombres, float previo1, float previo2, float previo3, float examenfinal) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.previo1 = previo1;
        this.previo2 = previo2;
        this.previo3 = previo3;
        this.examenfinal = examenfinal;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public float getPrevio1() {
        return previo1;
    }

    public void setPrevio1(float previo1) {
        this.previo1 = previo1;
    }

    public float getPrevio2() {
        return previo2;
    }

    public void setPrevio2(float previo2) {
        this.previo2 = previo2;
    }

    public float getPrevio3() {
        return previo3;
    }

    public void setPrevio3(float previo3) {
        this.previo3 = previo3;
    }

    public float getExamenfinal() {
        return examenfinal;
    }

    public void setExamenfinal(float examenfinal) {
        this.examenfinal = examenfinal;
    }
    
    private float getNotaFinal(){
        float first = ((this.getPrevio1() + this.getPrevio2() + this.getPrevio3()) / 3) * 7 / 10;
        float second = this.getExamenfinal() * 3 / 10;
        
        return first + second;
    }
    
    public boolean aprueba(){
        return this.getNotaFinal() >= 3;
    }

    @Override
    public boolean equals(Object obj){  // Override al método equals para comparar objetos de Tipo Estudiante
        if(obj instanceof Estudiante){
            Estudiante other  = (Estudiante)obj;
            return this.getCodigo() == other.getCodigo() && this.getNombres().equals(other.getNombres());
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombres=" + nombres + ", previo1=" + previo1 + ", previo2=" + previo2 + ", previo3=" + previo3 + ", examenfinal=" + examenfinal + '}';
    }
    
    
    
}
