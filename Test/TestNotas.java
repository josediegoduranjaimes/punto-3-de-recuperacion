/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Modelo.Estudiante;
import Negocio.Listacd;
import Negocio.Pila;
import Negocio.SistemaNotas;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Lenny
 */
public class TestNotas {
    public static void main(String[] args) throws Exception {
        SistemaNotas sis = new SistemaNotas();
        Scanner sc = new Scanner(System.in);
        
        List<Estudiante> list = new ArrayList<>();
        
        list.add(new Estudiante(1150810, "Estudiante 1", 3, 3, 3, 3));
        list.add(new Estudiante(1150805, "Estudiante 2", 3, 3, 3, 3));
        list.add(new Estudiante(1150807, "Estudiante 3", 2, 3, 2, 2));
        list.add(new Estudiante(1150808, "Estudiante 4", 3,0,0,0));
        list.add(new Estudiante(1150802, "Estudiante 5", 3, 1, 2, 3));
        
        Pila<Estudiante> ingles = new Pila<>();
        Pila<Estudiante> programacion = new Pila<>();
        Pila<Estudiante> calculo = new Pila<>();
        
        // Test donde los estudiantes solo están en 2 materias
        
        //sis.dosMaterias(ingles, programacion, calculo, list); // Excepción --> Las listas están vacías
        
        /* Creación de pilas */
        
        sis.crearPilaEstudiantes(list, ingles);
        sis.crearPilaEstudiantes(list, programacion);
        sis.crearPilaEstudiantes(list, calculo);
        
        /* Fin creación pilas */
        
        sis.reprobaronTodas(ingles, programacion, calculo, list);  // Estudiantes que pierden las 3 materias
        
        // Test de dos materias
        
        ingles.pop(); // Para retornar algún valor en el método de las 2 materias
        programacion.pop();
        
        System.out.println("************************");
        
        Listacd lista = sis.aprobaronTodas(ingles, programacion, calculo, list); // Test para los que aprueban las 3 materias
        System.out.println("Estudiantes que aprobaron las 3 materias");
        System.out.println(lista);
        System.out.println("************************");
        
        sis.dosMaterias(ingles, programacion, calculo, list); // Nombres de los que están matriculados en 2 materias
        System.out.println("************************");
        
        sis.unaMateria(ingles, programacion, calculo, list); // Estudiantes que están matriculados en 1 sola materia
        System.out.println("************************");
        
        /* Dejar las pilas tal cual estaban antes */
        ingles.push(new Estudiante(1150802, "Estudiante 5", 3, 1, 2, 3));
        programacion.push(new Estudiante(1150802, "Estudiante 5", 3, 1, 2, 3));
        
        Listacd listaIngles = sis.crearListaCD(ingles);
        System.out.println("Estudiantes que perdieron el p1 pero no el p2");
        System.out.println(listaIngles);
        System.out.println("************************");
        
    }
}
